package main

import (
	"fmt"
	"math/rand"
	"time"
)

type LinRegParams struct {
	b, a float64
}

func step(b_current float64, a_current float64,
	x_values []float64, y_values []float64, learning_rate float64) (float64, float64) {
	// Initialization
	var b_gradient float64 = 0
	var a_gradient float64 = 0
	var length = len(y_values)
	for i := 0; i < length; i++ {
		// Initialization
		var two_over_n = float64(2) / float64(length)
		b_gradient += -two_over_n * (y_values[i] - a_current*x_values[i] - b_current)
		a_gradient += -two_over_n * x_values[i] * (y_values[i] - a_current*x_values[i] - b_current)
	}
	var new_b = b_current - (learning_rate * b_gradient)
	var new_a = a_current - (learning_rate * a_gradient)
	return new_b, new_a
}

func Regression(x_values []float64, y_values []float64, learning_rate float64, channel chan LinRegParams, endChannel chan bool) (float64, float64) {
	if len(x_values) == 0 || len(y_values) == 0 {
		panic("Input arrays must not be empty.")
	}
	// Initialization
	var b_current float64 = 0
	var a_current float64 = 0
	var newba LinRegParams
	// IterationS of the gradient step
	for i := 0; i < 100000; i++ {
		b_current, a_current = step(b_current, a_current, x_values, y_values, learning_rate)
		remote := <-channel
		newba.b = 0.5*b_current + 0.5*remote.b
		newba.a = 0.5*a_current + 0.5*remote.a
		channel <- newba
	}
	endChannel <- true
	return b_current, a_current
}

func main() {
	c := make(chan LinRegParams, 10)
	end := make(chan bool)
	var True_regressor float64 = 2
	var True_intercept float64 = 5
	var number_data int = 1000
	x_values_node_1 := make([]float64, number_data)
	y_values_node_1 := make([]float64, number_data)
	x_values_node_2 := make([]float64, number_data)
	y_values_node_2 := make([]float64, number_data)
	x_values_node_3 := make([]float64, number_data)
	y_values_node_3 := make([]float64, number_data)

	// Generate pseudo-random source (set of random numbers) with an always different seed which is the current time in nanoseconds
	s1 := rand.NewSource(time.Now().UnixNano())
	// Generate a pseudo-random number using the source s1 numbers
	r1 := rand.New(s1)
	for i := 0; i < number_data; i++ {
		var rnd1 = r1.NormFloat64()
		var rnd2 = r1.NormFloat64()
		var rnd3 = r1.NormFloat64()
		x_values_node_1[i] = r1.Float64()
		y_values_node_1[i] = True_regressor*x_values_node_1[i] + True_intercept + rnd1
		x_values_node_2[i] = r1.Float64()
		y_values_node_2[i] = True_regressor*x_values_node_2[i] + True_intercept + rnd2
		x_values_node_3[i] = r1.Float64()
		y_values_node_3[i] = True_regressor*x_values_node_3[i] + True_intercept + rnd3
	}

	c <- LinRegParams{0, 0}
	go Regression(x_values_node_1, y_values_node_1, 0.1, c, end)
	go Regression(x_values_node_2, y_values_node_2, 0.2, c, end)
	go Regression(x_values_node_3, y_values_node_3, 0.3, c, end)

	<-end
	<-end
	<-end
	fmt.Println(<-c)
}
