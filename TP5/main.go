package main

import (
	"fmt"
	"math/rand"
	"time"
)

func node(i int, channel chan []int, beta float64, isByzantine bool) {
	for {
		currentState := <-channel
		if currentState[5] == 1 {
			if isByzantine {
				sum := 0
				for _, v := range currentState {
					sum += v
				}
				if sum >= 2 {
					currentState[i] = 0
				} else {
					currentState[i] = 1
				}
			} else {
				nodes := rand.Perm(5)[0:3]
				eta := (float64(currentState[nodes[0]]) + float64(currentState[nodes[1]]) + float64(currentState[nodes[2]])) / 3
				random := rand.Float64()
				U := random*float64(1-2*beta) + float64(beta)
				switch {
				case eta > U:
					currentState[i] = 1
				case eta < U:
					currentState[i] = 0
				}
			}
		}
		currentState[5] = 0
		channel <- currentState
	}
}

func main() {
	state := []int{1, 0, 1, 0, 1, 1}
	beta := 0.25
	consensusReached := false

	channel1 := make(chan []int)
	channel2 := make(chan []int)
	channel3 := make(chan []int)
	channel4 := make(chan []int)
	channel5 := make(chan []int)

	go node(0, channel1, beta, false)
	go node(1, channel2, beta, false)
	go node(2, channel3, beta, false)
	go node(3, channel4, beta, false)
	go node(4, channel5, beta, false)

	start := time.Now()

	for i := 0; i < 10000; i++ {
		stateCopy1 := make([]int, 6)
		stateCopy2 := make([]int, 6)
		stateCopy3 := make([]int, 6)
		stateCopy4 := make([]int, 6)
		stateCopy5 := make([]int, 6)

		copy(stateCopy1, state)
		copy(stateCopy2, state)
		copy(stateCopy3, state)
		copy(stateCopy4, state)
		copy(stateCopy5, state)

		channel1 <- stateCopy1
		channel2 <- stateCopy2
		channel3 <- stateCopy3
		channel4 <- stateCopy4
		channel5 <- stateCopy5

		stateNode1 := <-channel1
		stateNode2 := <-channel2
		stateNode3 := <-channel3
		stateNode4 := <-channel4
		stateNode5 := <-channel5

		if stateNode1[5] == 0 &&
			stateNode2[5] == 0 &&
			stateNode3[5] == 0 &&
			stateNode4[5] == 0 &&
			stateNode5[5] == 0 {
			state[0] = stateNode1[0]
			state[1] = stateNode2[1]
			state[2] = stateNode3[2]
			state[3] = stateNode4[3]
			state[4] = stateNode5[4]
			state[5] = 1
		}

		if state[0] == state[1] &&
			state[0] == state[2] &&
			state[0] == state[3] &&
			state[0] == state[4] {
			consensusReached = true
			break
		}
	}

	if consensusReached {
		fmt.Print("Consensus reached :")
	} else {
		fmt.Print("Consensus not reached :")
	}

	fmt.Println(state[:5])
	fmt.Println("Elapsed time :", time.Since(start))
}
