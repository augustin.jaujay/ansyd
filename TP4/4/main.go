package main

import (
	"fmt"
	"time"
)

type Communications struct {
	Jobs_ij      chan int
	Jobs_ji      chan int
	ACK_ij       chan int
	ACK_ji       chan int
	Heartbeat_ij chan int
	Heartbeat_ji chan int
	Node_i       int
	Node_j       int
}

func sending_job(id int, Interarrival_time int, Jobs_Sent chan int) {
	i := 0
	for {
		Jobs_Sent <- i
		fmt.Println(id, "sent job", i)
		time.Sleep(100 * time.Millisecond)
	}
}

func handling_jobs(id int, Jobs_Received chan int) {
	for {
		select {
		case i := <-Jobs_Received:
			fmt.Println(id, "received job", i)
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func answering_heartbeat(id int, Heartbeat_received chan int, ACK_Sent chan int) {
	for {
		select {
		case <-Heartbeat_received:
			fmt.Println(id, "received heartbeat")
			time.Sleep(time.Duration(id) * time.Second)
			ACK_Sent <- id
		}
	}
}

func failure_detector(id int, Heartbeat_Sent chan int, ACK_Received chan int) {
	var j int = 1
	var estimate_time float64 = 1
	var average_response_time float64 = 1
	var i float64 = 1
	Heartbeat_Sent <- j
	for {
		select {
		case <-time.After(100):
			estimate_time = estimate_time + 1
		case j := <-ACK_Received:
			i = i + 1
			average_response_time = average_response_time + (estimate_time-average_response_time)/i
			fmt.Println(id, "response time :", average_response_time)
			estimate_time = 1
			Heartbeat_Sent <- j + 1
		}
	}
}

func Node(id int,
	Interarrival_time int,
	Jobs_Sent chan int,
	Jobs_Received chan int,
	Heartbeat_Sent chan int,
	ACK_Received chan int,
	Heartbeat_Received chan int,
	ACK_Sent chan int) {
	go sending_job(id, Interarrival_time, Jobs_Sent)
	go handling_jobs(id, Jobs_Received)
	go failure_detector(id, Heartbeat_Sent, ACK_Received)
	go answering_heartbeat(id, Heartbeat_Received, ACK_Sent)
}

func main() {
	var listCommunications []Communications
	list_Nodes_i := make([]int, 2)
	list_Nodes_j := make([]int, 2)

	list_Nodes_i[0] = 1
	list_Nodes_i[1] = 2
	list_Nodes_j[0] = 2
	list_Nodes_j[1] = 3

	for i := range list_Nodes_i {
		var tmpCommunications Communications
		tmpCommunications.Jobs_ij = make(chan int, 5)
		tmpCommunications.Jobs_ji = make(chan int, 5)
		tmpCommunications.Heartbeat_ij = make(chan int, 5)
		tmpCommunications.Heartbeat_ji = make(chan int, 5)
		tmpCommunications.ACK_ij = make(chan int, 5)
		tmpCommunications.ACK_ji = make(chan int, 5)
		tmpCommunications.Node_i = list_Nodes_i[i]
		tmpCommunications.Node_j = list_Nodes_j[i]
		listCommunications = append(listCommunications, tmpCommunications)
		go Node(list_Nodes_i[i], 1, tmpCommunications.Jobs_ij, tmpCommunications.Jobs_ji, tmpCommunications.Heartbeat_ij, tmpCommunications.ACK_ji, tmpCommunications.Heartbeat_ji, tmpCommunications.ACK_ij)
		go Node(list_Nodes_j[i], 1, tmpCommunications.Jobs_ji, tmpCommunications.Jobs_ij, tmpCommunications.Heartbeat_ji, tmpCommunications.ACK_ij, tmpCommunications.Heartbeat_ij, tmpCommunications.ACK_ji)
	}

	time.Sleep(time.Duration(10) * time.Second)
}
