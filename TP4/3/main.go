package main

import (
	"fmt"
	"time"
)

func main() {
	jobs_1 := make(chan int, 5)
	heartbeat_1 := make(chan int)

	done := make(chan bool)
	go func() {
		i := 1
		for {
			select {
			case j, more := <-jobs_1:
				if more {
					fmt.Println("Node 1 received job", j)
				} else {
					fmt.Println("Node 1 received all jobs")
					done <- true
					return
				}
			case j, more := <-heartbeat_1:
				if more {
					fmt.Println("Node1 received the heartbeat", j)
					time.Sleep(time.Duration(i) * time.Second)
					heartbeat_1 <- i
					i = i + 1
				} else {
					fmt.Println("I'm not receiving jobs")
					done <- true
					return
				}
			}
		}
	}()

	heartbeat_1 <- 0
	for j := 1; j <= 10; j++ {
		if j%2 == 1 {
			jobs_1 <- j
			fmt.Println("sent job", j)
		} else {
			select {
			case <-heartbeat_1:
				heartbeat_1 <- j
			case <-time.After(2 * time.Second):
				fmt.Println("C'est tout cassé")
				return
			}
		}
	}
	close(jobs_1)
	fmt.Println("sent all jobs")
}
