package main

import "fmt"

func main1() {
	jobs_1 := make(chan int, 5)
	jobs_2 := make(chan int, 5)
	done_1 := make(chan bool)
	done_2 := make(chan bool)

	go func() {
		for {
			j, more := <-jobs_1
			if more {
				fmt.Println("1 received job", j)
			} else {
				fmt.Println("1 received all jobs")
				done_1 <- true
				return
			}
		}
	}()

	go func() {
		for {
			j, more := <-jobs_2
			if more {
				fmt.Println("2 received job", j)
			} else {
				fmt.Println("2 received all jobs")
				done_2 <- true
				return
			}
		}
	}()

	for j := 1; j <= 3; j++ {
		jobs_1 <- j
		jobs_2 <- j
		fmt.Println("sent job", j)
	}
	close(jobs_1)
	close(jobs_2)
	fmt.Println("sent all jobs")

	<-done_1
	<-done_2
}

func main() {
	jobs := make(chan int, 5)
	done_1 := make(chan bool)
	done_2 := make(chan bool)

	go func() {
		received := []int{0, 0, 0}
		for {
			j, more := <-jobs
			isJReceived := false
			for _, v := range received {
				if j == v {
					isJReceived = true
				}
			}
			if more && !isJReceived {
				fmt.Println("1 received job", j)
				received[j-1] = j
				jobs <- j
			} else {
				fmt.Println("1 received all jobs")
				done_1 <- true
				return
			}
		}
	}()

	go func() {
		received := []int{0, 0, 0}
		for {
			j, more := <-jobs
			isJReceived := false
			for _, v := range received {
				if j == v {
					isJReceived = true
				}
			}
			if more && !isJReceived {
				fmt.Println("2 received job", j)
				received[j-1] = j
				jobs <- j
			} else {
				fmt.Println("2 received all jobs")
				done_2 <- true
				return
			}
		}
	}()

	for j := 1; j <= 3; j++ {
		jobs <- j
		fmt.Println("sent job", j)
	}
	// close(jobs)
	fmt.Println("sent all jobs")

	<-done_1
	<-done_2
}
