package main

import (
	"fmt"
	"math/rand"
	"net"
	"time"
)

func sendResponse(conn *net.UDPConn, addr *net.UDPAddr) {
	_, err := conn.WriteToUDP([]byte("Client test"), addr)
	if err != nil {
		fmt.Printf("Couldn’t send response %v", err)
	}
}

func main() {
	p := make([]byte, 2048)
	addr := net.UDPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 1234}
	for {
		ser, err := net.ListenUDP("udp", &addr)
		if err != nil {
			fmt.Printf("Some error %v\n", err)
			return
		}
		for {
			_, remoteaddr, err := ser.ReadFromUDP(p)
			fmt.Printf("Read a message from %v : %s \n", remoteaddr, p)
			if err != nil {
				continue
			}
			time.Sleep(time.Duration(rand.Intn(10000)) * time.Millisecond)
			go sendResponse(ser, remoteaddr)
		}
	}
}
