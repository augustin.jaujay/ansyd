import numpy as np
import matplotlib.pyplot as plt
import csv

simLen = 100
X = np.linspace(0, simLen, simLen * 10)
Y = np.sin(X)
noise = np.random.normal(0, 0.1, len(X))
Y = Y + noise

file = open('sinus.csv', 'w')
writer = csv.writer(file)
for i in range(len(X)):
    writer.writerow([X[i], Y[i]])

file.close()

plt.plot(X, Y)
plt.show()