package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

func node(i int, channel chan int) {

}

func main() {
	channel0 := make(chan int)
	channel1 := make(chan int)
	channel2 := make(chan int)
	channel3 := make(chan int)
	channel4 := make(chan int)

	go node(0, channel0)
	go node(1, channel1)
	go node(2, channel2)
	go node(3, channel3)
	go node(4, channel4)

	file, _ := os.Open("sinus.csv")
	lines, _ := csv.NewReader(file).ReadAll()
	fmt.Println(lines)
}
