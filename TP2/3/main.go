package main

type Message struct {
	terminate bool
	max       int
}

func node1link(id int, channelin chan *Message, channelout chan *Message, Decision chan *Message) {
	State := new(Message)
	State.terminate = false
	State.max = id
	Counter := 1
	for {
		select {
		case Input := <-channelin:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channelout <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channelout <- State
				State.terminate = true
				Decision <- State
			}
		default:
			channelout <- State
		}
		Counter++
	}
}

func node2links(id int,
	channel1in chan *Message, channel2in chan *Message,
	channel1out chan *Message, channel2out chan *Message,
	Decision chan *Message) {
	State := new(Message)
	State.terminate = false
	State.max = id
	Counter := 1
	for {
		select {
		case Input := <-channel1in:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channel1out <- State
				channel2out <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channel1out <- State
				channel2out <- State
				State.terminate = true
				Decision <- State
			}
		case Input := <-channel2in:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channel1out <- State
				channel2out <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channel1out <- State
				channel2out <- State
				State.terminate = true
				Decision <- State
			}
		default:
			channel1out <- State
			channel2out <- State
		}
		Counter++
	}
}

func node3links(id int,
	channel1in chan *Message, channel2in chan *Message, channel3in chan *Message,
	channel1out chan *Message, channel2out chan *Message, channel3out chan *Message,
	Decision chan *Message) {
	State := new(Message)
	State.terminate = false
	State.max = id
	Counter := 1
	for {
		select {
		case Input := <-channel1in:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channel1out <- State
				channel2out <- State
				channel3out <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channel1out <- State
				channel2out <- State
				channel3out <- State
				State.terminate = true
				Decision <- State
			}
		case Input := <-channel2in:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channel1out <- State
				channel2out <- State
				channel3out <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channel1out <- State
				channel2out <- State
				channel3out <- State
				State.terminate = true
				Decision <- State
			}
		case Input := <-channel3in:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channel1out <- State
				channel2out <- State
				channel3out <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channel1out <- State
				channel2out <- State
				channel3out <- State
				State.terminate = true
				Decision <- State
			}
		default:
			channel1out <- State
			channel2out <- State
			channel3out <- State
		}
		Counter++
	}
}

func main() {
	channel12 := make(chan *Message, 10)
	channel23 := make(chan *Message, 10)
	channel34 := make(chan *Message, 10)
	channel35 := make(chan *Message, 10)
	channel45 := make(chan *Message, 10)
	channel21 := make(chan *Message, 10)
	channel32 := make(chan *Message, 10)
	channel43 := make(chan *Message, 10)
	channel53 := make(chan *Message, 10)
	channel54 := make(chan *Message, 10)
	decision1 := make(chan *Message)
	decision2 := make(chan *Message)
	decision3 := make(chan *Message)
	decision4 := make(chan *Message)
	decision5 := make(chan *Message)
	go node1link(1, channel21, channel12, decision1)
	go node2links(2, channel12, channel32, channel21, channel23, decision2)
	go node3links(3, channel23, channel43, channel53, channel32, channel34, channel35, decision3)
	go node2links(4, channel34, channel54, channel43, channel45, decision4)
	go node2links(5, channel35, channel45, channel53, channel54, decision5)
	select {
	case Input := <-decision1:
		print("Decision node 1 : ")
		println(Input.max)
	case Input := <-decision2:
		print("Decision node 2 : ")
		println(Input.max)
	case Input := <-decision3:
		print("Decision node 3 : ")
		println(Input.max)
	case Input := <-decision4:
		print("Decision node 4 : ")
		println(Input.max)
	case Input := <-decision5:
		print("Decision node 5 : ")
		println(Input.max)
	}
}
