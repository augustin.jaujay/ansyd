package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Ball struct{ hits int }

func main() {
	linkAB := make(chan *Ball)
	linkBC := make(chan *Ball)
	linkCD := make(chan *Ball)
	linkBD := make(chan *Ball)
	go player1link("Alice", linkAB, 100)
	go player3links("Bob", linkAB, linkBC, linkBD, 200)
	go player2links("Chloe", linkBC, linkCD, 300)
	go player2links("David", linkCD, linkBD, 400)
	linkAB <- new(Ball) // game on; toss the ball
	time.Sleep(3 * time.Second)
}

func player1link(name string, table chan *Ball, sleepTime time.Duration) {
	for {
		ball := <-table
		ball.hits++
		fmt.Println(name, ball.hits)
		time.Sleep(sleepTime * time.Millisecond)
		table <- ball
	}
}

func player2links(name string, table1 chan *Ball, table2 chan *Ball, sleepTime time.Duration) {
	for {
		var ball *Ball
		select {
		case m := <-table1:
			ball = m
		case m := <-table2:
			ball = m
		}
		ball.hits++
		fmt.Println(name, ball.hits)
		time.Sleep(sleepTime * time.Millisecond)
		r := rand.Intn(2)
		if r == 0 {
			table1 <- ball
		}
		if r == 1 {
			table2 <- ball
		}
	}
}

func player3links(name string, table1 chan *Ball, table2 chan *Ball, table3 chan *Ball, sleepTime time.Duration) {
	for {
		var ball *Ball
		select {
		case m := <-table1:
			ball = m
		case m := <-table2:
			ball = m
		case m := <-table3:
			ball = m
		}
		ball.hits++
		fmt.Println(name, ball.hits)
		time.Sleep(sleepTime * time.Millisecond)
		r := rand.Intn(3)
		if r == 0 {
			table1 <- ball
		}
		if r == 1 {
			table2 <- ball
		}
		if r == 2 {
			table3 <- ball
		}
	}
}
