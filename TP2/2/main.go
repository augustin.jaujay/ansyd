package main

type Message struct {
	terminate bool
	max       int
}

func node(id int, channelinput chan *Message, channeloutput chan *Message, Decision chan *Message) {
	State := new(Message)
	State.terminate = false
	State.max = id
	Counter := 1
	for {
		select {
		case Input := <-channelinput:
			switch {
			case Input.max > State.max:
				State.max = Input.max
				channeloutput <- State
			case Input.max < State.max:
			case Input.max == State.max:
				channeloutput <- State
				State.terminate = true
				Decision <- State
			}
		default:
			channeloutput <- State
		}
		Counter++
	}
}

func main() {
	channel1 := make(chan *Message)
	channel2 := make(chan *Message)
	channel3 := make(chan *Message)
	decision1 := make(chan *Message)
	decision2 := make(chan *Message)
	decision3 := make(chan *Message)
	go node(1, channel3, channel1, decision1) //Add all the channels
	go node(2, channel1, channel2, decision2) //Add all the channels
	go node(3, channel2, channel3, decision3) //Add all the channels
	select {
	case Input := <-decision1:
		print("Decision node 1 : ")
		println(Input.max)
	case Input := <-decision2:
		print("Decision node 2 : ")
		println(Input.max)
	case Input := <-decision3:
		print("Decision node 3 : ")
		println(Input.max)
	}
}
