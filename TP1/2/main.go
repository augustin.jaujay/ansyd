package main

import (
	"fmt"
)

type Node struct {
	name   string
	vector []float64
}

func main() {
	node1 := Node{
		"Alice",
		[]float64{1.1, 2.2},
	}
	fmt.Println("Node name : ", node1.name)
	for _, v := range node1.vector {
		fmt.Printf("\t %v\n", v)
	}
}
