package main

import "fmt"

type Data struct {
	x []int
}

func increment(listToAdd []int, c chan *Data) {
	for {
		// var t *Data
		t := <-c //receive
		for i := range t.x {
			t.x[i] += listToAdd[i] //increment
		}
		c <- t
		//send it back
	}
}

func printList(l []int) {
	for _, v := range l {
		fmt.Printf("%v ", v)
	}
	println()
}

func main() {
	c := make(chan *Data)
	t := Data{[]int{1, 2, 3}}
	go increment([]int{4, 5, 6}, c)
	printList(t.x)
	c <- &t  //send a pointer to t
	y := <-c //receive the result
	printList(y.x)
}
